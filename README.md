# Chat application

## Backend

```sh
$ cd backend
$ npm start
```

## Frontend

### Proof of Concept

```sh
$ cd web/poc
$ http-server
```
