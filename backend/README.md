# backend

a [Sails](http://sailsjs.org) application


## Relationships

Channel 1:M Event
Server 1:M User
Event 1:1 User
Channel 1:M User


## Models

User
- nick

Channel
- name
- userIds

Event
- channelId
- userId
- type (enum)
- value


## Features

Channel
- list
- create

Event
- add
- edit
- remove

Authentication (Waterlock)
- create
- login
- logout
- jwt




var channelInfo = {
  channelId: Int,
  name: String,
  users: Int
};

var channel = {
  id: Int,
  name: String,
  users: Array<User>
}

var event = {
  id: Int,
  channelId: Int,
  user: User,
  type: String,  
}

Get channel list



Get channel



Get channel events



// waterlock handled

Get users

Register user

Login user

Logout user

Get user JWT token
