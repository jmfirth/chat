/**
* Event.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    user: {
      type: 'User'
    },
    type: {
      type: 'string',
      enum: ['message', 'join', 'part'],
      defaultsTo: 'message'
    },
    value: {
      type: 'string'
    }
  }
};
